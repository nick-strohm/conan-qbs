import os
from conans import ConanFile, tools
from conans.errors import ConanInvalidConfiguration


class QbsConan(ConanFile):
    name = "Qbs"
    version = "1.17.0"
    license = "LGPL-3.0"
    author = "Kai Dohmen psykai1993@googlemail.com"
    url = "https://gitlab.com/Psy-Kai/conan-qbs"
    description = "Qbs is a build automation tool designed to conveniently "
    "manage the build process of software projects across "
    "multiple platforms. Qbs can be used for any software "
    "project, regardless of programming language, toolkit, "
    "or libraries used."
    topics = ("conan", "qbs", "build-system")
    settings = "os", "arch"

    def configure(self):
        if self.settings.os != "Windows" and self.settings.os != "Linux":
            raise ConanInvalidConfiguration(
                "Only windows and linux supported for Qbs")
        if self.settings.arch != "x86_64":
            raise ConanInvalidConfiguration(
                "Only x86_64 systems supported for Qbs")

    @property
    def _os_str(self):
        return str(self.settings.os).lower()

    @property
    def _archive_type(self):
        if self.settings.os == "Windows":
            return "zip"
        if self.settings.os == "Linux":
            return "tar.gz"

    @property
    def _qbs_folder_name(self):
        return "qbs-%s-x86_64-%s" % (self._os_str, self.version)

    @property
    def _qbs_archive_name(self):
        return "%s.%s" % (self._qbs_folder_name, self._archive_type)

    def build(self):
        link = "http://download.qt.io/official_releases/qbs/%s/%s" % (
            self.version, self._qbs_archive_name)
        tools.download(link, self._qbs_archive_name)

        self.output.info("Downloading Qbs: %s" % (link))
        tools.unzip(self._qbs_archive_name)
        os.unlink(self._qbs_archive_name)

    def package(self):
        self.copy("*", src=self._qbs_folder_name, keep_path=True)

    def package_info(self):
        qbs_root = os.path.join(self.package_folder, "bin")
        self.output.info("Appending PATH environment variable: %s" % (
            qbs_root))
        self.env_info.PATH.append(qbs_root)
